#--------------------------------------------------------------#
#
#		Process and plots of the plankton data from
#		the VISUFRONT cruise in July 2013
#		Robin Faillettaz - 2013/12/10
# 
#--------------------------------------------------------------#


library("ggplot2")
library("plyr")
library("dplyr")
library("stringr")
library("reshape2")

source("lib_process.R")



# set options for ploting
opts <- theme(axis.title.y= element_text(angle=90, vjust=0.5, size=18), axis.title.x= element_text(angle=0, vjust=0.5, size=18)) + 
theme(axis.text.x  = element_text(angle=0, vjust=0.5, size=15), axis.text.y  = element_text(angle=0, vjust=0.5, size=15))




# For now locate data from the dropbox repo manually
dir <- "/Users/faillettaz/Dropbox/visufront-data/"

# read coastline to plot the trajectories and check
coast <- read.csv("data/cote_azur.csv")
load("data/coast_bathy.RData")

# read stations position
station <- read.csv(str_c(dir, "_raw_/nets/station-regent-visufront.csv"), header=T, sep=";")
head(station)

# Remove station for which positions were wrong
station <- filter(station, !is.na(lat_out))

# compute lat and lon for stations
latBits <- str_split_fixed(station$lat_out, fixed("."), 2)
station$lat_out <- as.numeric(latBits[,1]) + (as.numeric(latBits[,2])/60)
station$lat_out <- 43 + station$lat_out/60
lonBits <- str_split_fixed(station$lon_out, fixed("."), 2)
station$lon_out <- as.numeric(lonBits[,1]) + (as.numeric(lonBits[,2])/60)
station$lon_out <- 7 + station$lon_out/60

latBits <- str_split_fixed(station$lat_in, fixed("."), 2)
station$lat_in <- as.numeric(latBits[,1]) + (as.numeric(latBits[,2])/60)
station$lat_in <- 43 + station$lat_in/60
lonBits <- str_split_fixed(station$lon_in, fixed("."), 2)
station$lon_in <- as.numeric(lonBits[,1]) + (as.numeric(lonBits[,2])/60)
station$lon_in <- 7 + station$lon_in/60


# Add Boussole as the reference point
Boussole <- data.frame(lat=43.38, lon=7.83)


# plot boat traj + stations
# read ship trajectory from ts
filenames <- list.files(str_c(dir, "_raw_/TS/"))

# Add position from TS data instead of logsheets
s <- adply(filenames, 1, function(x) {
	s <- read.ts(str_c(dir, "_raw_/TS/",x))
	return(s)
	}, .progress="text")
head(s)

# Compute mean position at each minute
s$datetime_round <- round_any(ymd_hms(s$dateTimeUTC), 60) + 7200
s <- group_by(s, datetime_round) %>% summarize(lon = mean(na.omit(lon)), lat = mean(na.omit(lat)))
head(s)
class(s$datetime_round)

s <- filter(s, datetime_round < ymd_hms("2013-07-19 13:00:00 UTC") | datetime_round > ymd_hms("2013-07-26 11:00:00 UTC") & datetime_round < ymd_hms("2013-07-26 16:10:00") | datetime_round > ymd_hms("2013-07-29 09:45:00 UTC") & datetime_round < ymd_hms("2013-07-29 11:15:00"))
s$transect <- NA
s$transect[which(s$dateTimeUTC < ymd_hms("2013-07-25 03:35:00 UTC"))] <- "t4"
s$transect[which(is.na(s$transect))] <- "t5"
head(s)


# check station traj
ggplot(station) + geom_segment(aes(x = lon_in, xend = lon_out, y = lat_in, yend = lat_out, group = station_name, color = station_name), size = 3) + geom_path(data = s, aes(x = lon, y = lat))

p <- ggplot() + 
	geom_polygon(data=coast, aes(x=lon, y=lat), fill="lightblue3") + 
	geom_contour(data = bathyDF, aes(x = x, y = y, z = z), color = "grey50", breaks = c(500, 1000, 1500, 2000, 2500)) +
	geom_point(aes(x=lon_in, y=lat_in), data=station, size=3, fill = "white", shape = 21) + 	geom_text(aes(x=lon_in, y=lat_in, label = station_nb), data=station, vjust = 0.02, hjust = 0.02) + 
	geom_segment(aes(x=lon_in, xend = lon_out, y=lat_in, yend = lat_out), color = "grey20", data=station, size=0.5) +
	coord_map(xlim=c(7.1,8.1), ylim=c(43.2,43.75)) + 	scale_x_continuous("Longitude") + scale_y_continuous("Latitude") + theme_bw() + opts
print(p)
# ggsave("plots/location_stations_nets.pdf", width = 10, height = 7)







###--------  LARVAL FISH ABUNDANCE AND DISTRIBUTION  -------###


# For fish larvae
fl <- read.csv("~/Dropbox/visufront-data/_raw_/nets/visufront-fish-larvae.csv", sep=";", h=T)
head(fl)

# Remove station 2 (Why?)
fl <- filter(fl, !station == "station_2")

length(unique(fl$station))

sum(station$larvae) # 675 fish larvae
sum(station$eggs) # 548 eggs

# prepare the data
length(unique(fl$order)) # 13 orders
length(unique(fl$family)) # 29 families
species <- unique(str_c(str_c(fl$genera, fl$sp, sep=" "), str_c(" (", fl$descriptor, ")")))
length(species)  # 46 species
species <- sort(species)
#write.table(species, "species-only.csv", row.names=F, col.names=F)
species <- read.csv("~/Dropbox/visufront-data/_raw_/nets/species.csv", h=T, sep=";")
species$species <- as.character(species$species)

# add a column with the species name in the fl
fl$species <- str_c(str_c(fl$genera, fl$sp, sep=" "), str_c(" (", fl$descriptor, ")"))

# add a column to the stations to join it to the fish abundance
station$station <- str_c("station_", station$station_nb)
d <- join(fl, station, by="station")
d <- join(d, species)
head(d)
length(unique(d$station))


length(unique(filter(species, habitat == "coastal")$species)) / length(unique(species$species))
# 41 % of coastal species
length(unique(filter(species, habitat == "pelagic")$species)) / length(unique(species$species))
# 15% of pelagic species
length(unique(filter(species, habitat == "mesopelagic")$species)) / length(unique(species$species))
# 37% of pelagic species
length(unique(filter(species, habitat == "benthopelagic")$species)) / length(unique(species$species))
# 4% of pelagic species



# abund <- ddply(d, .(station, habitat), summarize, sum=sum(larvae_nb), abundance=sum(larvae_nb)/(vol_out[1]-vol_in[1]) * 10000, order=length(unique(order)), family=length(unique(family)), species=length(unique(sp)), lat=lat[1], lon=lon[1], date=date[1])
# head(abund)
#
# for fish larvae RAW ABUNDANCE





#------------------------------------------
# Compute abundance per volume unit
#------------------------------------------


# 2nd method = compute sampled volume using speed and distance
#------------------------------------------------------------------
# select variables  of lon and lat
dstation <- ddply(d, ~station, function(x){
    data.frame(lat_in=unique(x$lat_in), lon_in=unique(x$lon_in),
    lat_out=unique(x$lat_out), lon_out=unique(x$lon_out), 
    time_in = (str_c(unique(x$date), " ", unique(x$time_start))),
    time_out = (str_c(unique(x$date), " ", unique(x$time_end))),
    larvae_nb = sum(x$larvae_nb))})
head(dstation)



library("oce")
# compute distance and time per station
dist_time <- ddply(dstation, ~station, function(x){
    difftime <- difftime(as.POSIXct(x$time_out), as.POSIXct(x$time_in))
    dist.m <- geodDist(lat1=x$lat_in, lon1=x$lon_in, lat2=x$lat_out, lon2=x$lon_out, alongPath=F)
    time <- as.numeric(difftime) * 60
    speed <- dist.m / time * 1000
    vol.s <- speed * pi * 1^2  # pi.r^2 = 0.78 m^2
    vol.m3 <- vol.s * time
		# If use middle of station position
		# longitude <- mean(x$lon_in, x$lon_out)
		# latitude <- mean(x$lat_in, x$lat_out)
		
		# or position in
		longitude <- x$lon_in
		latitude <- x$lat_in
    data.frame(longitude, latitude, vol.m3)
	})


# join station sampled volume and larval abundances
abund.m3 <- join(dstation, dist_time)
abund.m3$abund.1000m3 <- abund.m3$larvae_nb / abund.m3$vol.m3 * 1000



# Plot fish larvae distribution
ggplot() + stat_contour(data=bathyDF, aes(z=-z, x=x, y=y), colour="gray80", size=0.3) + geom_polygon(fill="lightblue3", data=coast, aes(x=lon, y=lat)) + geom_point(aes(x=lon_out, y=lat_out, size=abund.1000m3), shape = 21, data=abund.m3) + scale_x_continuous("Longitude", expand=c(0,0)) + scale_y_continuous("Latitude", expand=c(0,0)) + scale_size(name = expression(paste("Fish larvae 1000m"^"-3")), range = c(1, 10)) + coord_quickmap(xlim=c(7.1, 8.05), ylim=c(43.24, 43.75)) + theme_bw() + opts
# nice plot
# ggsave("plots/plkt-nets/distrib-allspecies.pdf", height = 6, width = 12)




# join vol sampled per station with eggs
head(station)
station <- join(station, dist_time)

# Plot fish eggs distribution
ggplot() + stat_contour(data=bathyDF, aes(z=-z, x=x, y=y), colour="gray80", size=0.3) + geom_polygon(fill="lightblue3", data=coast, aes(x=lon, y=lat)) + geom_point(aes(x=lon_out, y=lat_out, size=eggs/vol.m3*1000), shape = 21, data=station) + scale_x_continuous("Longitude", expand=c(0,0)) + scale_y_continuous("Latitude", expand=c(0,0)) + scale_size(name = expression(paste("Fish eggs 1000m"^"-3")), range = c(1, 10)) + coord_quickmap(xlim=c(7.1, 8.05), ylim=c(43.24, 43.75)) + theme_bw() + opts
# nice plot
# ggsave("plots/plkt-nets/distrib-allspecies-eggs.pdf", height = 6, width = 12)






# Do it per habitat
# --------------------------------------------------------------------
# select variables  of lon and lat
dstation <- ddply(d, ~station+habitat, function(x){
    data.frame(lat_in=unique(x$lat_in), lon_in=unique(x$lon_in),
    lat_out=unique(x$lat_out), lon_out=unique(x$lon_out), 
    time_in = (str_c(unique(x$date), " ", unique(x$time_start))),
    time_out = (str_c(unique(x$date), " ", unique(x$time_end))),
    larvae_nb = sum(x$larvae_nb)) })
head(dstation)


# compute distance and time per station
dist_time <- ddply(dstation, ~station+habitat, function(x){
    difftime <- difftime(as.POSIXct(x$time_out), as.POSIXct(x$time_in))
    dist.m <- geodDist(lat1=x$lat_in, lon1=x$lon_in, lat2=x$lat_out, lon2=x$lon_out, alongPath=F)
    time <- as.numeric(difftime) * 60
    speed <- dist.m / time * 1000
    vol.s <- speed * pi * 1^2  # pi.r^2 = 0.78 m^2
    vol.m3 <- vol.s * time
		# If use middle of station position
		# longitude <- mean(x$lon_in, x$lon_out)
		# latitude <- mean(x$lat_in, x$lat_out)
		
		# Adding depth adds only ~4m-3 per 1000m-3
		
		# or position in
		longitude <- x$lon_in
		latitude <- x$lat_in
    data.frame(longitude, latitude, vol.m3)
	})


# join station sampled volume and larval abundances
abund.m3 <- join(dstation, dist_time)
abund.m3$abund.1000m3 <- abund.m3$larvae_nb / abund.m3$vol.m3 * 1000

head(abund.m3)
group_by(abund.m3, habitat) %>% summarise(min = min(abund.1000m3), max = max(abund.1000m3), mean = mean(abund.1000m3))
#         habitat       min      max      mean
# 1       coastal 0.1709531 1.425340 0.7401860
# 2       pelagic 0.1214842 9.320155 3.4439806
# 3 benthopelagic 0.1002167 0.142534 0.1213753
# 4   mesopelagic 0.1681272 8.518422 1.9099264
# 5            NA 0.1681272 1.403034 0.4587621



abund.m3$habitat <- factor(abund.m3$habitat, levels = c("coastal", "pelagic", "benthopelagic", "mesopelagic"))
# Plot fish larvae distribution
ggplot() + stat_contour(data=bathyDF, aes(z=-z, x=x, y=y), colour="gray80", size=0.3) + geom_polygon(fill="lightblue3", data=coast, aes(x=lon, y=lat)) + geom_point(aes(x=lon_out, y=lat_out, size=abund.1000m3), shape = 21, data=filter(abund.m3, !habitat %in% c("unidentified"))) + scale_x_continuous("Longitude", expand=c(0,0)) + scale_y_continuous("Latitude", expand=c(0,0)) + scale_size(name = expression(paste("Fish larvae 1000m"^"-3")), range = c(1, 10)) + coord_quickmap(xlim=c(7.1, 8.05), ylim=c(43.24, 43.75)) + theme_bw() + opts + facet_wrap(~habitat)

# Very nice plot with the four habitat. Good. 
ggsave("plots/plkt-nets/distrib-fishlarvae-habitats.pdf", width = 12, height = 8.5)






# Do it per species
# --------------------------------------------------------------------
# select variables  of lon and lat
dstation <- ddply(d, ~station+habitat+species, function(x){
    data.frame(lat_in=unique(x$lat_in), lon_in=unique(x$lon_in),
    lat_out=unique(x$lat_out), lon_out=unique(x$lon_out), 
    time_in = (str_c(unique(x$date), " ", unique(x$time_start))),
    time_out = (str_c(unique(x$date), " ", unique(x$time_end))),
    larvae_nb = sum(x$larvae_nb)) })
head(dstation)


# compute distance and time per station
dist_time <- ddply(dstation, ~station+habitat+species, function(x){
    difftime <- difftime(as.POSIXct(x$time_out), as.POSIXct(x$time_in))
    dist.m <- geodDist(lat1=x$lat_in, lon1=x$lon_in, lat2=x$lat_out, lon2=x$lon_out, alongPath=F)
    time <- as.numeric(difftime) * 60
    speed <- dist.m / time * 1000
    vol.s <- speed * pi * 1^2  # pi.r^2 = 0.78 m^2
    vol.m3 <- vol.s * time
		# If use middle of station position
		# longitude <- mean(x$lon_in, x$lon_out)
		# latitude <- mean(x$lat_in, x$lat_out)
		
		# Adding depth adds only ~4m-3 per 1000m-3
		
		# or position in
		longitude <- x$lon_in
		latitude <- x$lat_in
    data.frame(longitude, latitude, vol.m3)
	})


# join station sampled volume and larval abundances
abund.m3 <- join(dstation, dist_time)
abund.m3$abund.1000m3 <- abund.m3$larvae_nb / abund.m3$vol.m3 * 1000

head(abund.m3)
group_by(abund.m3, species) %>% summarise(min = min(abund.1000m3), max = max(abund.1000m3), mean = mean(abund.1000m3))
#         habitat       min      max      mean
# 1       coastal 0.1709531 1.425340 0.7401860
# 2       pelagic 0.1214842 9.320155 3.4439806
# 3 benthopelagic 0.1002167 0.142534 0.1213753
# 4   mesopelagic 0.1681272 8.518422 1.9099264
# 5            NA 0.1681272 1.403034 0.4587621


fishab <- abund.m3
fishab$dist_from_vlfr <- geodDist(lon1 = rep(7.31, nrow(fishab)), lat1 = rep(43.68, nrow(fishab)), lon2 = fishab$lon_in, lat2 = fishab$lat_in)

# Project the location of each point over the transect axis, with lm
t1lat <- lm(latitude~dist_from_vlfr, data = fishab)
library("broom")
t1lat <- augment(t1lat)$.fitted
fishab$lat_lm <- t1lat
t1lon <- lm(longitude~dist_from_vlfr, data = fishab)
t1lon <- augment(t1lon)$.fitted
fishab$lon_lm <- t1lon


head(fishab)
fishab$dist_vlfr_round <- round_any(fishab$dist_from_vlfr, 1)

fishab2 <- ddply(fishab, ~habitat+species+dist_vlfr_round, summarise, abund_km = mean(abund.1000m3))


fishab2$habitat <- factor(fishab2$habitat, levels = c("coastal", "unidentified", "mesopelagic", "pelagic", "benthopelagic"))

# Plot fish larvae distribution
ggplot() + geom_bar(aes(x=dist_vlfr_round, y=abund_km, fill=habitat), data=fishab2, stat = 'identity', pos = "stack", width = 0.8, color = "grey20", size = 0.3) + scale_x_continuous("Distance from Villefranche (km)", limits = c(-1, 63), expand = c(0, 0)) + scale_y_continuous(expression(paste("Larvae 1000m"^"-3"))) + scale_fill_discrete("Habitats") + theme_bw() + opts
ggsave("plots/plkt-nets/assemblages_per_habitat.pdf", width = 12, height = 7)

ggplot() + geom_bar(aes(x=dist_from_vlfr, y=abund.1000m3, fill=species), data=fishab, stat = 'identity', pos = "stack", width = 0.5) + scale_x_continuous("Distance from Villefranche", limits = c(-1, 63), expand = c(0, 0)) + scale_y_continuous("Larvae 1000m-3") + theme_bw() + opts + facet_wrap(~habitat, scale = "free_y")
ggsave("plots/plkt-nets/assemblages_per_facethabitat.pdf")


# Add variables
station <- left_join(station, unique(select(fishab, station, dist_from_vlfr)))



# --------------------------------------------------------------------
#       Multivariate analysis
# --------------------------------------------------------------------


# Read CTD from ISIIS
# --------------------------------------------------------------------

# Need transects CC5 (for stations on 26th) and CC7 (for stations on 29th) and glider for stations on 18th and 19th

# Read physical data from transect 4
phyt4 <- read.csv(str_c(dir, "transects/cross_current_5/isiis.csv"), header=T, sep=",")
head(phyt4)
# delete first line (data from previous transect)
phyt4 <- phyt4[-1, ]
phyt4$transect <- "t5"


phyt5 <- read.csv(str_c(dir, "transects/cross_current_7/isiis.csv"), header=T, sep=",")
head(phyt5)
phyt5$transect <- "t7"
unique(select(phyt5, cast, profile))
# Profiles are fine, but casts are reverted

# Join the two 
phy <- rbind(phyt4, phyt5)
head(phy)

# Change names to same number of character for each (for plots)
phy <- rename(phy, Temp.celsius = Temp.C)

# select only data from upcasts --> Keep first down cast below 25m to improve interpolation
d <- filter(phy, down.up == "up" | (down.up == "down" & Depth.m > 28), !is.na(Salinity.PPT), Salinity.PPT > 37.5)

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# NEED TO INVERT CAST NUMBER IN CC5
ggplot(d, aes(x=dist_from_vlfr, y=-Depth.m, colour=factor(cast)))  + geom_point() + facet_wrap(~transect)
# OKAY 


# compute interpolation 
# interpolate all variables
dm <- melt(d, id.vars=c("Depth.m", "dist_from_vlfr", "transect"), measure.vars=c("Salinity.PPT", "Temp.celsius", "Fluoro.volts", "Oxygen.ml.l"))


source("lib_process.R")
source("lib_plot.R")

# First interpolation over a large grid
di <- ddply(dm, ~transect+variable, function(x) {
    x <- na.omit(x)
    xi <- interp.dist(x=x$dist_from_vlfr, y=x$Depth.m, z=x$value, duplicate="mean", x.step=500, y.step=2.5, anisotropy=1300)
}, .progress="text")
di <- rename(di, distance = x, Depth.m  = y)


# GLIDER
# --------------------------------------------------------------------
# Read data
d <- read.csv("data/cleaned_glider_visufront_data.csv", h = T)
head(d)

ggplot(filter(d, !is.na(salinity)), aes(x = dist_from_vlfr, y = -ipressure)) + geom_point(aes(color = salinity))



# Smooth data
# interpolate linearly
# Remove NAs and first depth bin
# d <- filter(d, !is.na(u), !Depth.m < 9)
head(d)



# Try to average per bin AND distance bins
d$ipressure_round <- round_any(d$ipressure, 5)
d$dist_round <- round_any(d$dist_from_vlfr, 2)
head(d)

dM <- melt(select(d, -present_time, -lat, -lon, -pressure, -ipressure, -dist_from_vlfr, -dateTime), id.vars = 
c("ipressure_round", "ilat", "ilon", "leg", "dist_round"))
dM$value <- as.numeric(dM$value)
head(dM)

tmp <- group_by(dM, variable, ipressure_round, dist_round) %>% summarize(value = mean(na.omit(value)))
head(tmp)
tail(tmp)

ggplot(filter(tmp, variable == "oxygen", !is.na(value)), aes(x = dist_round, y = -ipressure_round)) + geom_point(aes(color = value)) + stat_contour(aes(z = value), breaks = c(38.2, 38.3))
# It works, but it's not great because horizontal lines are generated 


i <- dlply(tmp, ~variable, function(x) {
	
	# x <- filter(dM, variable == "salinity")
	x <- filter(x, !is.na(value))
	# dim(x)
	# ggplot(x, aes(x = dist_round, y = -ipressure_round)) + geom_point(aes(color = value))
	
	# Create the grid for the nex interpolation w/ extrapolation
	yt <- seq(0, 500, by=5)
	xt <- seq(3, max(x$dist_round), length=length(yt))
	(grid <- data.frame(x=xt, y=yt))

	# any(is.na(x))
	i <- interp(x=x$dist_round, y=x$ipressure_round, z=x$value, xo=xt, yo=yt, duplicate = "mean", extrap = F, linear = T)
	return(i)
}, .progress = "text")

# Create grid
yt <- seq(0, 500, by=1)
xt <- seq(3, max(d$dist_round), length=length(yt))
(grid <- data.frame(x=xt, y=yt))


library("fields")
out <- ldply(i, function(x) {
	smooth <- image.smooth(x, grid=grid, theta=1)
	# pass the list to df
	out <- melt(smooth$z, varnames=c("x","y"))
	out$x <- smooth$x[out$x]
	out$y <- smooth$y[out$y]
	out <- rename(out, distance=x, Depth.m=y)
	return(out)
}, .progress = "text")
head(out)

# plot it
out <- filter(out, !variable %in% c("conductivity", "density"))
out$variable <- factor(out$variable, levels = c("temperature", "salinity", "oxygen", "chlorophyll"))
unique(out$variable)
out$transect <- "g1"

out$variable <- as.character(out$variable)
out$variable[which(out$variable == "oxygen")] <- "Oxygen.ml.l"
out$variable[which(out$variable == "salinity")] <- "Salinity.PPT"
out$variable[which(out$variable == "temperature")] <- "Temp.celsius"
out$variable[which(out$variable == "chlorophyll")] <- "Fluoro.volts"
head(out)
head(di)

out <- select(out, transect, variable, distance, Depth.m, value)

# Join ISIIS and glider data
ctd <- rbind(di, out)
head(ctd)




# Extract relevant CTD values 
unique(ctd$transect)
station


station$transect <- NA
station$transect[which(station$station_nb %in% 1:11)] <- "g1"
station$transect[which(station$station_nb %in% 12:18)] <- "t5"
station$transect[which(station$station_nb %in% 19:20)] <- "t7"


# round dist
station$distR <- round_any(station$dist_from_vlfr, 0.8)
ctd$distR <- round_any(ctd$distance, 0.8)


ctdF <- ddply(ctd, ~transect+variable+distR, function(x) {
	var.surf = mean(na.omit(filter(x, Depth.m < 10)$value))
	var.100m = mean(na.omit(filter(x, Depth.m > 95 & Depth.m < 105)$value))
	var.50m = mean(na.omit(filter(x, Depth.m > 45 & Depth.m < 55)$value))
	var.mean = mean(na.omit(filter(x, Depth.m < 95)$value))
	data.frame(var.mean = var.mean, var.surf = var.surf, var.50m = var.50m, var.100m = var.100m)
}, .progress = "text")
head(ctdF)


ctdL <- dlply(ctdF, ~transect + variable, function(x) {
	var = unique(x$variable)
	if (var == "Salinity.PPT") {
	x <- rename(x, sal.mean = var.mean, sal.surf = var.surf, sal.50m = var.50m, sal.100m = var.100m)
	}
	if (var == "Temp.celsius") {
	x <- rename(x, temp.mean = var.mean, temp.surf = var.surf, temp.50m = var.50m, temp.100m = var.100m)
	}
	if (var == "Oxygen.ml.l") {
	x <- rename(x, oxygen.mean = var.mean, oxygen.surf = var.surf, oxygen.50m = var.50m, oxygen.100m = var.100m)
	}
	if (var == "Fluoro.volts") {
	x <- rename(x, fluoro.mean = var.mean, fluoro.surf = var.surf, fluoro.50m = var.50m, fluoro.100m = var.100m)
	}
	return(x)
})

g1 <- cbind(select(ctdL$g1.Salinity.PPT, -variable), select(ctdL$g1.Temp.celsius, -variable, -transect, -distR), select(ctdL$g1.Oxygen.ml.l, -variable, -transect, -distR), select(ctdL$g1.Fluoro.volts, -variable, -transect, -distR))
head(g1)
t5 <- cbind(select(ctdL$t5.Salinity.PPT, -variable), select(ctdL$t5.Temp.celsius, -variable, -transect, -distR), select(ctdL$t5.Oxygen.ml.l, -variable, -transect, -distR), select(ctdL$t5.Fluoro.volts, -variable, -transect, -distR))
t7 <- cbind(select(ctdL$t7.Salinity.PPT, -variable), select(ctdL$t7.Temp.celsius, -variable, -transect, -distR), select(ctdL$t7.Oxygen.ml.l, -variable, -transect, -distR), select(ctdL$t7.Fluoro.volts, -variable, -transect, -distR))

ctdAll <- rbind(g1, t5, t7)
head(ctdAll)

station <- left_join(station, ctdAll)
station


# Prepare data for CCA
# --------------------------------------------------------------------

head(fishab)

dC <- fishab
# dC$abund.1000m3 <- log(dC$abund.1000m3+1)
dC <- dcast(station~species, data = fishab, value.var = "abund.1000m3")

# Replace NA with zeros in all columns
dC[, 2:ncol(dC)][is.na(dC[, 2:ncol(dC)])] <- 0
head(dC)


head(station)
head(dC)
names(station)


# Add coastal/frontal/offshore
station$zone <- NA
station$zone[which((station$transect != "g1" & station$distR <= 26) | (station$transect == "g1" & station$distR <= 18))] <- "peripheral"
station$zone[which((station$transect != "g1" & station$distR > 26  & station$distR <= 39) | (station$transect == "g1" & station$distR > 18  & station$distR <= 30))] <- "frontal"
station$zone[which((station$transect != "g1" & station$distR > 39) | (station$transect == "g1" & station$distR > 30))] <- "central"

dCCA <- left_join(dC[, -33], station[, c(25, 16, 31:48)])
head(dCCA)
names(dCCA)


# save(station, dCCA, file = "data/station_dCCA.RData")
load("data/station_dCCA.RData")

names(dCCA)
rownames(dCCA) <- dCCA$station
CA <- dCCA[, c(2:46)]
order <- melt(sort(colSums(CA), decreasing = T))
CA <- CA[ ,row.names(order)]

# Read shortnames
shortnames <- read.csv("data/sp_shortnames.csv", h = T, sep = ";")
data.frame(as.character(sort(shortnames$species)), sort(colnames(CA)))
colnames(CA) <- shortnames$shortname
names(dCCA)

CA <- cbind(CA, dCCA[, c(51, 63, 55, 65)])
CA$zone <- factor(CA$zone)
names(CA)
library("FactoMineR")
res <- CA(X = CA, quanti.sup = 46:48, quali.sup = 49)


sp_hab <- join(data.frame(group_by(fishab, species) %>% summarise(sum = sum(abund.1000m3)) %>% arrange(-sum)), species) %>% filter(!species == "Octopus sp (Cuvier, 1798)")



# ggplot()+geom_point(aes(x=res$col$coord[,1], y=res$col$coord[,2], size=c(res$col$cos2[,1])), shape=2, color="red", fill=1)+
# geom_text(aes(x=res$col$coord[,1], y=res$col$coord[,2],label=rownames(res$col$coord)), vjust=1.5, color="red")+
# geom_point(aes(x=res$row$coord[,1], y=res$row$coord[,2], size=c(res$row$cos2[,1])), color="black")+
# geom_text(aes(x=res$row$coord[,1], y=res$row$coord[,2], label=rownames(res$row$coord)), vjust=-1.5, col="black")+
# geom_vline(aes(x=0), linetype=2, size=0.6)+
# geom_hline(aes(y=0), linetype=2, size=0.6)+
# scale_x_continuous(name=paste("Dim 1(", sprintf("%.2f",res$eig[1,2]),"%)"), limits = c(-2, 2))+
# scale_y_continuous(name=paste("Dim 2(", sprintf("%.2f",res$eig[2,2]),"%)"), limits = c(-2, 4))+
# scale_size_continuous(name="cos²")


sp_hab$habitat <- factor(sp_hab$habitat, levels = c("coastal", "unidentified", "mesopelagic", "pelagic", "benthopelagic"))


ggplot() + geom_point(aes(x=res$col$coord[,1], y=res$col$coord[,2], size=c(res$col$cos2[,1]), color = sp_hab$habitat), size = 3) + geom_text(aes(x=res$col$coord[,1], y=res$col$coord[,2], label=rownames(res$col$coord), vjust = -0.8, color = sp_hab$habitat), size = 4) + geom_point(aes(x = res$quali.sup$coord[, 1], y = res$quali.sup$coord[, 2]), shape = 2, color = "grey20", size = 5) + geom_text(aes(x = res$quali.sup$coord[, 1], y = res$quali.sup$coord[, 2], label = rownames(res$quali.sup$coord), vjust = -1.2), color = "grey20", size = 4)+ theme_bw() + geom_vline(aes(x=0), linetype=2, size=0.5)+ geom_hline(aes(y=0), linetype=2, size=0.5) + 
# geom_segment(aes(x=0, xend=res$quanti.sup$coord[, 1], y=0, yend=res$quanti.sup$coord[,2]), arrow=arrow(length= unit(0.2,"cm"), type="closed"), color = "grey20", size = .5) + geom_text(aes(x = res$quanti.sup$coord[, 1], y = res$quanti.sup$coord[, 2], label = rownames(res$quanti.sup$coord)), color = "grey20") +
 scale_x_continuous(name=paste("Dim 1(", sprintf("%.2f",res$eig[1,2]),"%)"), limits = c(-1.8, 1.2))+ scale_y_continuous(name=paste("Dim 2(", sprintf("%.2f",res$eig[2,2]),"%)"), limits = c(-1.5, 3.5)) + scale_color_discrete("Habitats")
ggsave("plots/plkt-nets/CA_habitats.pdf")


# But try CCA on all species to see if it's better or worse
# --------------------------------------------------------------------
Xall <- dCCA[, c(2:46)]
order <- melt(sort(colSums(Xall), decreasing = T))
Xall <- Xall[ ,row.names(order)]

# Read shortnames
shortnames <- read.csv("data/sp_shortnames.csv", h = T, sep = ";")
shortnames <- filter(shortnames, !shortname == "Octopus")
colnames(Xall) <- shortnames$shortname
Yall <- dCCA[, c(1, 47:ncol(dCCA))]
names(Yall)

library("vegan")
library("grid")
# (res.cca <- cca(Xall~bottom_depth+distR+sal.mean+sal.surf+temp.mean+temp.surf+oxygen.mean+oxygen.surf+fluoro.mean+fluoro.surf, data = Yall))
(res.cca <- cca(Xall~bottom_depth+distR+sal.mean+sal.surf+sal.50m+temp.mean+temp.50m+temp.surf+oxygen.mean+oxygen.surf+oxygen.50m+fluoro.50m+zone, data = Yall))


# Constrained inertia is lower than for abundant species only
plot(res.cca)


# test significativity of the CCA
anova(res.cca)
anova.cca(res.cca, by = "axis", permutations = 500)


# Backward selection of explanatory variables of the CCA with function ordistep()
cca.step.forward <- ordistep(cca(Xall~zone, data = Yall), scope=formula(res.cca), direction="forward", pstep=1000)
# cca.step.forward <- ordistep(cca(Xall~bottom_depth+distR+sal.mean+sal.surf+sal.50m+sal.100m+temp.mean+temp.50m+temp.100m+temp.surf+oxygen.mean+oxygen.surf+oxygen.50m+oxygen.100m+fluoro.mean+fluoro.surf+fluoro.50m+fluoro.100m, data = Yall), scope=formula(res.cca), direction="forward", pstep=500)


# New CCA with relevant var only
(res.cca.part <- cca(Xall ~ zone, data = Yall))
# Call: cca(formula = Xall ~ zone + fluoro.50m + sal.surf + oxygen.50m,
# data = Yall)
#
#               Inertia Proportion Rank
# Total          2.1237     1.0000
# Constrained    1.1189     0.5268    5
# Unconstrained  1.0049     0.4732   13
# Inertia is mean squared contingency coefficient
#
# Eigenvalues for constrained axes:
#   CCA1   CCA2   CCA3   CCA4   CCA5
# 0.4439 0.2710 0.1930 0.1645 0.0466
#
# Eigenvalues for unconstrained axes:
#     CA1     CA2     CA3     CA4     CA5     CA6     CA7     CA8     CA9    CA10
# 0.22074 0.18606 0.15311 0.09860 0.09378 0.07234 0.05553 0.05142 0.04335 0.01557
#    CA11    CA12    CA13
# 0.00839 0.00382 0.00215


# (res.cca.part <- cca(X ~ season + month + temp + copepoda_temora + egg + crust_nauplii + appendicularia + egg_fish_egg + sal + crust_euphausiids_larvae + gelatinous_sipho_cloche + temp_prev2wk + O2_prev2wk + crust_other_larvae + fluo_prev2wk + copepoda_acartia + radiolaria, data = Y, scale = 10))
anova(res.cca.part)
# Permutation test for cca under reduced model
# Permutation: free
# Number of permutations: 999
#
# Model: cca(formula = Xall ~ zone + fluoro.50m + sal.surf + oxygen.50m, data = Yall)
#          Df ChiSquare      F Pr(>F)
# Model     5    1.1189 2.8951  0.001 ***
# Residual 13    1.0049
# ---
# Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’
# anova.cca(res.cca.part, by = "axis", permutations = 500)


# Plot CCA
plot(res.cca.part)



# use the package function "scores" to extract positions
species.res <- as.data.frame(scores(res.cca.part)$species)
var.res <- as.data.frame(res.cca.part$CCA$biplot[, 1:2])
var.imp <- as.data.frame(res.cca.part$CCA$envcenter)

 # Constrained Inertia = 0.4446
# p = 0.002
# And the results are very similar to the CCA with selected species only, 
# So it's better to take them all, makes more sense.


# Try to make a nicer CCA plot
ggplot()+ geom_point(aes(x=CCA1, y=CCA2), data=species.res, colour = "coral2", size = 3, shape = 8) + geom_text(aes(x = CCA1, y = CCA2, label = row.names(species.res), vjust = -0.8), size = 4, data = species.res, color = "coral2") + geom_segment(aes(x=0, xend=CCA1*1, y=0, yend=CCA2*1), arrow=arrow(length= unit(0.2,"cm"), type="closed"), data=var.res, color = "dodgerblue3", size = .5) + geom_text(aes(x = CCA1*1.1, y = CCA2*1.1, label = rownames(var.res)), data = var.res, color = "dodgerblue3") + theme_bw() + geom_vline(aes(x=0), linetype=2, size=0.5) + geom_hline(aes(y=0), linetype=2, size=0.5) 
# + scale_x_continuous("CCA1", limits = c(-1.8, 1.5)) + scale_y_continuous("CCA2", limits = c(-2, 1))
# ggsave("plots/plkt-nets/CCA_allfish_spp_shortnames44.4442.pdf")



